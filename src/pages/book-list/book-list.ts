import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { DatasService } from '../../services/datas.service';
import { LendBookPage } from './lend-book/lend-book';
import { Book } from '../../models/Book';

@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})
export class BookListPage {

  constructor(
    private modalCtrl: ModalController,
    private datasService: DatasService,private menuCtrl:MenuController
  ) {}

  ionViewWillEnter(){
      this.booksList = this.datasService.booksList.slice();
  }
  onLoadBook(index:number) {
    let modal = this.modalCtrl.create(LendBookPage, {
      index: index
    });
    modal.present();
  }
  onToggleMenu(){
      this.menuCtrl.open();
  }
  booksList: Book[];
}
