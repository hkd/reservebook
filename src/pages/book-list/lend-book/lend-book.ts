import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Book } from '../../../models/Book';
import { DatasService } from '../../../services/datas.service';

@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html',
})
export class LendBookPage {

  index:number;
  book: Book;
  constructor(public navParams: NavParams,public viewCtrl:ViewController,
    public datasService:DatasService) {
  }

  ngOnInit() {
    this.index = this.navParams.get('index');
    this.book = this.datasService.booksList[this.index];
  }

  dismissModal(){
    this.viewCtrl.dismiss();
  }
  onToggleBook(){
    this.book.isHere=!this.book.isHere;
  }
}
