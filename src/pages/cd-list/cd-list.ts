import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { DatasService } from '../../services/datas.service';
import { LendCdPage } from './lend-cd/lend-cd';
import { Cd } from '../../models/CD';

@Component({
  selector: 'page-cd-list',
  templateUrl: 'cd-list.html',
})
export class CdListPage {

  constructor(
    private modalCtrl: ModalController,
    private datasService: DatasService,private menuCtrl:MenuController
  ) {}

  ionViewWillEnter(){
      this.cdsList = this.datasService.cdsList.slice();
  }
  onLoadCd(index:number) {
    let modal = this.modalCtrl.create(LendCdPage, {
      index: index
    });
    modal.present();
  }
  onToggleMenu(){
      this.menuCtrl.open();
  }
  cdsList: Cd[];

}
