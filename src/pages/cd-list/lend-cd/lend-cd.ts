import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Cd } from '../../../models/CD';
import { DatasService } from '../../../services/datas.service';

@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage {

  index:number;
  cd: Cd;
  constructor(public navParams: NavParams,public viewCtrl:ViewController,
    public datasService:DatasService) {
  }

  ngOnInit() {
    this.index = this.navParams.get('index');
    this.cd = this.datasService.cdsList[this.index];
  }

  dismissModal(){
    this.viewCtrl.dismiss();
  }
  onToggleCd(){
    this.cd.isHere=!this.cd.isHere;
  }
}
