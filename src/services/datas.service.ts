import { Cd } from "../models/CD";
import { Book } from "../models/Book";

export class DatasService {
  cdsList: Cd[] = [
    {
      name: "DAVID LE COMBATTANT",
      isHere: false,
      description:
        "Sa séparation avec son alter ego a contribué son succès cette année malgré son déboire il a gardé la tête haute et sert ses fans majoritairement féminins. Il est plébiscité aux Kundes à venir avec"
    },
    {
      name: "ALY VERHUTHEY",
      isHere: true,
      description: "Burn of third degree of ankle"
    },
    {
      name: "Dj Barsa 1er",
      isHere: false,
      description: "Sixième de ce classement,  il a fait vibrer les cœurs avec la chorale  avec lui c’est toujours la grande messe pour la délivrance. Il a fait sorti beaucoup d’ondes cet été et la délivrance continuera."
    },
    {
      name: "MTY LA MERVEILLE",
      isHere: false,
      description: "Unspecified perforation of tympanic membrane, right ear"
    },
    {
      name: "Bébéto Bongo",
      isHere: true,
      description: "Quand le Burkina-Faso s’adapte et comprend les exigences du showbiz actuel et l’ouverture à d’autres tendances, Bebeto crée une nouvelle danse : le Zoun­gou Zoungou. Le succès aurait été planétaire ou du moins continental si la promotion autour du titre n’a pas connu quelques burg. En attendant de voir l’effet du remix avec Awilo Longomba. Bébéto Bongo s’en sort 3e de ce classement. Bravo à l’artiste."
    },
    {
      name: "Smarty",
      isHere: true,
      description:
        "Voilà l’intrus de notre classement il est venu boulversé la donne à 3 jours du verdict avec l’effet bombe de son dernier « ModeAvion » avec tout le buzz qu’à susciter le clip vidéo. Voilà un artiste qui est à su aller au delà de l’imagination avec les réseaux sociaux."
    },
    {
      name: "Imilo le chanceux",
      isHere: true,
      description:
      "Après un premier album couronné de succès,  Emile Ilboudo n’a pas arrêté d’enchaîner les tubes. Son nouvel album sorti en 2015, « Dieu est ma Force » avec Wassa Wassa ou encore Talaba le dernier né  n’a pas dérogé à la règle avec plusieurs places de numéro 1 dans 6 pays.  Jeune décalé du Faso il a tout explosé en 2015 : tout ce qu'il touche ça marche et ça devrait continuer en 2016."
    },
    {
      name: "FLOBY",
      isHere: false,
      description:
        "Posterior dislocation of right acromioclavicular joint, initial encounter"
    },
    {
      name: "Amadou Balake",
      isHere: false,
      description: "Patient's noncompliance with medical treatment and regimen"
    },
    {
      name: "Alif Naaba",
      isHere: false,
      description:
        "Toxic effect of other specified noxious substances eaten as food, accidental (unintentional), initial encounter"
    }
  ];
  booksList: Book[] = [
    {
      name: "Tout s'effondre",
      isHere: true,
      description: "Release of chordee"
    },
    {
      name: "Pétales de sang",
      isHere: true,
      description:
        "Microscopic examination of specimen from skin and other integument, parasitology"
    },
    {
      name: "Les Soleils des indépendances",
      isHere: false,
      description: "Simple mastoidectomy"
    },
    {
      name: "En attendant le vote des bêtes sauvages ",
      isHere: false,
      description: "Fasciotomy of hand"
    },
    {
      name: "Allah n'est pas obligé",
      isHere: false,
      description: "Biopsy of joint structure, other specified sites"
    },
    {
      name: "Monnè, outrages et défis",
      isHere: true,
      description: "Color vision study"
    },
    { name: "Comedy|Drama", isHere: true, description: "Cardiac mapping" },
    {
      name: "Il te faut partir à l'aube ",
      isHere: true,
      description: "Other laparoscopic umbilical herniorrhaphy"
    },
    {
      name: "Aké, les années d'enfance",
      isHere: true,
      description: "Implantation of diaphragmatic pacemaker"
    },
    {
      name: "Le ventre",
      isHere: false,
      description: "Stereotactic radiosurgery, not otherwise specified"
    }
  ];
}
