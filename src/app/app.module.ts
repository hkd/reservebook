import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BookListPage } from '../pages/book-list/book-list';
import { LendBookPage } from '../pages/book-list/lend-book/lend-book';
import { CdListPage } from '../pages/cd-list/cd-list';
import { LendCdPage } from '../pages/cd-list/lend-cd/lend-cd';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingPage } from '../pages/setting/setting';
import { DatasService } from '../services/datas.service';

@NgModule({
  declarations: [
    MyApp,BookListPage,LendBookPage,CdListPage,LendCdPage,TabsPage,SettingPage,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,BookListPage,LendBookPage,CdListPage,LendCdPage,TabsPage,SettingPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,DatasService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
